<?php

use App\Http\Controllers\CouponApiController;
use App\Http\Controllers\CouponShopController;
use App\Http\Controllers\ShopApiController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::resource('coupons', CouponApiController::class)->only(['index']);
Route::post('{admin}/coupons', [CouponApiController::class, 'store']);
Route::get('coupons/{id}', [CouponApiController::class, 'show']);
Route::put('coupons/{id}', [CouponApiController::class, 'update']);
Route::delete('coupons/{id}', [CouponApiController::class, 'destroy']);

Route::resource('shops', ShopApiController::class)->only(['index']);
Route::post('{admin}/shops', [ShopApiController::class, 'store']);
Route::get('shops/{id}', [ShopApiController::class, 'show']);
Route::put('shops/{id}', [ShopApiController::class, 'update']);
Route::delete('shops/{id}', [ShopApiController::class, 'destroy']);

Route::get('coupons-lists', [CouponShopController::class, 'index']);
Route::post('store-coupons-shops', [CouponShopController::class, 'store']);
Route::get('coupons/{coupon_id}/shops/{shop_id}', [CouponShopController::class, 'show']);
Route::delete('coupons/{coupon_id}/shops/{shop_id}', [CouponShopController::class, 'destroy']);
