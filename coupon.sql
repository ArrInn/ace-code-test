# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.5.5-10.4.6-MariaDB)
# Database: ace_test
# Generation Time: 2021-02-27 9:23:52 AM +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table admins
# ------------------------------------------------------------

DROP TABLE IF EXISTS `admins`;

CREATE TABLE `admins` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uuid` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(128) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(128) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(64) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `admin_uuid_unique` (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `admins` WRITE;
/*!40000 ALTER TABLE `admins` DISABLE KEYS */;

INSERT INTO `admins` (`id`, `uuid`, `username`, `email`, `password`, `deleted_at`, `created_at`, `updated_at`)
VALUES
	(1,'9317be85-24da-3c67-a2a9-9c57614d6fd0','Admin test','testadmin@gmail.com','$2y$10$X065XGWFOOYtcz5HMs13S.XET2Iw6kSDWwMS8S3WJuBvysi8JUGba',NULL,'2006-04-20 02:09:54','1998-04-10 03:02:13'),
	(2,'85d73e59-1479-374b-8221-b4d7028f0c7f','Admin test1','testadmin@gmail.com','$2y$10$ry23VT.l2uYDdWvpDxw92O9UdZOOehUDcw8WFDhbtB2qFENbljZei',NULL,'1991-12-14 16:30:23','1997-12-02 00:17:31'),
	(3,'3922b029-642b-3431-8a1f-0431f7ec0ced','Ms. Darby Kirlin IV','huel.dessie@hotmail.com','$2y$10$3lc.jNiNywsQDB2TdEsnju6lEpHGm6CFV4m3mfi4B3Kt/Xu2PhZfa',NULL,'2018-11-08 14:16:37','2001-07-12 09:48:14'),
	(4,'88a3f4f9-0f7a-3d22-a95f-514abda559e7','Jaiden Corkery','roselyn61@yahoo.com','$2y$10$.M1Tf6w.KwfBF.IQ.CDQaushYc/JJtwPxosDq9ZrkHZKml5LrwB8O',NULL,'1991-08-21 13:49:45','2006-10-02 15:48:34'),
	(5,'948e3e48-22db-315f-9def-956836e2d349','Willard Dooley','wilkinson.lazaro@yahoo.com','$2y$10$i3RGBEC2ynGXmE0D2GG5z.Z0ebZ819Ru/rtBil6rF64w1puksi1wm',NULL,'2008-07-02 08:49:19','2017-08-10 22:25:25');

/*!40000 ALTER TABLE `admins` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table coupon_shops
# ------------------------------------------------------------

DROP TABLE IF EXISTS `coupon_shops`;

CREATE TABLE `coupon_shops` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `coupon_id` int(10) unsigned NOT NULL,
  `shop_id` int(10) unsigned NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `coupon_shops_coupon_id_foreign` (`coupon_id`),
  KEY `coupon_shops_shop_id_foreign` (`shop_id`),
  CONSTRAINT `coupon_shops_coupon_id_foreign` FOREIGN KEY (`coupon_id`) REFERENCES `coupons` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `coupon_shops_shop_id_foreign` FOREIGN KEY (`shop_id`) REFERENCES `shops` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table coupons
# ------------------------------------------------------------

DROP TABLE IF EXISTS `coupons`;

CREATE TABLE `coupons` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `admin_id` int(10) unsigned NOT NULL,
  `name` varchar(128) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `discount_type` enum('percentage','fix-amount') COLLATE utf8mb4_unicode_ci NOT NULL,
  `amount` int(11) NOT NULL,
  `image_url` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `code` int(11) NOT NULL DEFAULT 0,
  `start_datetime` datetime DEFAULT NULL,
  `end_datetime` datetime DEFAULT NULL,
  `coupon_type` enum('private','public') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'public',
  `used_count` int(10) unsigned NOT NULL DEFAULT 0,
  `deleted_at` datetime DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `coupons_admin_id_foreign` (`admin_id`),
  KEY `idx_name` (`name`),
  KEY `idx_discount_type` (`discount_type`),
  CONSTRAINT `coupons_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table shops
# ------------------------------------------------------------

DROP TABLE IF EXISTS `shops`;

CREATE TABLE `shops` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `admin_id` int(10) unsigned NOT NULL,
  `name` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `query` varchar(64) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `latitude` decimal(10,8) NOT NULL DEFAULT 0.00000000,
  `longitude` decimal(10,8) NOT NULL DEFAULT 0.00000000,
  `zoom` int(10) unsigned DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `shops_admin_id_foreign` (`admin_id`),
  CONSTRAINT `shops_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;




/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
