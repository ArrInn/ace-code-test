<?php

namespace App\Services;

use App\Models\Coupon;

class CouponService
{
    public static function store($request, $admin)
    {
        $coupon = new Coupon();
        $coupon->admin_id = $admin->id;
        $coupon->name = $request->name;
        $coupon->description = $request->description;
        $coupon->discount_type = $request->discount_type;
        $coupon->amount = $request->amount;
        $coupon->image_url = $request->image_url;
        $coupon->code = $request->code;
        $coupon->start_datetime = $request->start_datetime;
        $coupon->end_datetime = $request->end_datetime;
        $coupon->coupon_type = $request->coupon_type;
        // $coupon->used_count = $request->used_count;
        $coupon->save();

        return $coupon;
    }

    public static function update($request, $id)
    {
        $coupon = Coupon::find($id);
        if ($coupon) {
            $coupon->update($request->all());
        }

        return $coupon;
    }

    public static function storeCouponShop($coupon, $shop)
    {
        $check = $coupon->shops()->where('shop_id', $shop->id)->exists();
        if ($check == true) {
            return ['code' => 409];
        }
        $couponShop = $coupon->shops()->attach($shop->id);

        return ['code' => 200];
    }
}
