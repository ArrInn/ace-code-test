<?php

namespace App\Services;

use App\Models\Shop;

class ShopService
{
    public static function store($request, $admin)
    {
        $shop = Shop::create([
            'admin_id' => $admin->id,
            'name' => $request['name'],
            'query' => $request['query'],
            'latitude' => $request['latitude'],
            'longitude' => $request['longitude'],
            'zoom' => $request['zoom'],
        ]);

        return $shop;
    }

    public static function update($request, $id)
    {
        $shop = Shop::find($id);
        if ($shop) {
            $shop->update($request->all());
        }

        return $shop;
    }
}
