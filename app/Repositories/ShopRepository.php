<?php

namespace App\Repositories;

use App\Models\Shop;

class ShopRepository
{
    public static function getShops($request)
    {
        $shops = Shop::whereName($request->name)->whereOffset($request->offset)->whereLimit($request->limit)->get();
        return [
            'shops' => $shops,
            'offset' => $request->offset ?? 0,
            'limit' => $request->limit ?? 30,
        ];
    }

    public static function getShopByID($id)
    {
        return Shop::find($id);
    }
}
