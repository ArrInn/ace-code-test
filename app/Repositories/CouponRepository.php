<?php

namespace App\Repositories;

use App\Models\Coupon;

class CouponRepository
{
    public static function getCoupons($request)
    {
        $coupons = Coupon::whereName($request->name)
            ->whereOffset($request->offset)
            ->whereLimit($request->limit)
            ->get();
        return [
            'coupons' => $coupons,
            'offset' => $request->offset ?? 0,
            'limit' => $request->limit ?? 30,
        ];
    }

    public static function getCouponByID(int $id)
    {
        return Coupon::find($id);
    }

    public static function getCouponShops($request)
    {
        $coupons = Coupon::whereId($request->coupon_id)
            ->whereOffset($request->offset)
            ->whereLimit($request->limit)
            ->get();

        $filterCoupons = $coupons->filter(function ($q) {
            return $q->shops->count() > 0;
        });

        return [
            'coupons' => $filterCoupons,
            'offset' => $request->offset ?? 0,
            'limit' => $request->limit ?? 30,
        ];
    }
}
