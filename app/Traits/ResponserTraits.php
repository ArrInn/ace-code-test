<?php

namespace App\Traits;

use Illuminate\Http\Response;

trait ResponserTraits
{
    public function respondListCollection(int $message, $data, $error, $duration, int $limit, int $offset)
    {
        return response()->json([
            'success' => $message,
            'code' => Response::HTTP_OK,
            'meta' => [
                'method' => request()->method(),
                'endpoint' => \Request::getRequestUri(),
                'limit' => $limit,
                'offset' => $offset,
                'total' => $data->count(),
            ],
            'data' => $data,
            'errors' => $error,
            'duration' => $duration
        ], Response::HTTP_OK);
    }

    public function respondSuccessCollection(int $message, $data, $error, $duration)
    {
        return response()->json([
            'success' => $message,
            'code' => Response::HTTP_OK,
            'meta' => [
                'method' => request()->method(),
                'endpoint' => \Request::getRequestUri(),
            ],
            'data' => $data,
            'errors' => $error,
            'duration' => $duration
        ], Response::HTTP_OK);
    }

    public function respondCreateCollection(int $message, $data, $error, $duration)
    {
        return response()->json([
            'success' => $message,
            'code' => Response::HTTP_CREATED,
            'meta' => [
                'method' => request()->method(),
                'endpoint' => \Request::getRequestUri(),
            ],
            'data' => $data,
            'errors' => $error,
            'duration' => $duration
        ], Response::HTTP_CREATED);
    }

    public function respondUpdateCollection(int $message, $data, $error, $duration)
    {
        return response()->json([
            'success' => $message,
            'code' => Response::HTTP_OK,
            'meta' => [
                'method' => request()->method(),
                'endpoint' => \Request::getRequestUri(),
            ],
            'data' => $data,
            'errors' => $error,
            'duration' => $duration
        ], Response::HTTP_OK);
    }

    public function respondValidationErrorCollection(int $message, $data, $error, $duration)
    {
        return response()->json([
            'success' => $message,
            'code' => Response::HTTP_BAD_REQUEST,
            'meta' => [
                'method' => request()->method(),
                'endpoint' => \Request::getRequestUri(),
            ],
            'data' => $data,
            'errors' => [
                'message' => "The request parameters are incorrect, please make sure to follow the documentation about request parameters of the resource.",
                'code' => 400002,
                'validation' => $error
            ],
            'duration' => $duration
        ], Response::HTTP_BAD_REQUEST);
    }

    public function respondNotFoundErrorCollection(int $message, $data, $error, $duration)
    {
        return response()->json([
            'success' => $message,
            'code' => Response::HTTP_NOT_FOUND,
            'meta' => [
                'method' => request()->method(),
                'endpoint' => \Request::getRequestUri(),
            ],
            'data' => $data,
            'errors' => [
                'message' => $error['message'],
                'code' => $error['code'],
            ],
            'duration' => $duration
        ], Response::HTTP_NOT_FOUND);
    }
}
