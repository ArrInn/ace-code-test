<?php

namespace App\Http\Resources;

use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class CouponShopListResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'admin_id' => $this->admin_id,
            'name' => $this->name,
            'description' => $this->description,
            'discount_type' => $this->discount_type,
            'amount' => $this->amount,
            'image_url' => $this->image_url,
            'code' => $this->code,
            'start_datetime' => $this->start_datetime,
            'end_datetime' => $this->end_datetime,
            'coupon_type' => $this->coupon_type,
            'used_count' => $this->used_count,
            'created_at' => Carbon::parse($this->created_at)->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::parse($this->updated_at)->format('Y-m-d H:i:s'),
            'shops' => ShopResource::collection($this->shops)
        ];
    }
}
