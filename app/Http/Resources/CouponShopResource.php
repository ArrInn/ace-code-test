<?php

namespace App\Http\Resources;

use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class CouponShopResource extends JsonResource
{
    private $coupon;
    private $shop;

    public function __construct($coupon, $shop)
    {
        $this->coupon = $coupon;
        $this->shop = $shop;
    }
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->coupon->id,
            'admin_id' => $this->coupon->admin_id,
            'name' => $this->coupon->name,
            'description' => $this->coupon->description,
            'discount_type' => $this->coupon->discount_type,
            'amount' => $this->coupon->amount,
            'image_url' => $this->coupon->image_url,
            'code' => $this->coupon->code,
            'start_datetime' => $this->coupon->start_datetime,
            'end_datetime' => $this->coupon->end_datetime,
            'coupon_type' => $this->coupon->coupon_type,
            'used_count' => $this->coupon->used_count,
            'created_at' => Carbon::parse($this->coupon->created_at)->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::parse($this->coupon->updated_at)->format('Y-m-d H:i:s'),
            'shops' => new ShopResource($this->shop)
        ];
    }
}
