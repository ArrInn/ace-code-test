<?php

namespace App\Http\Controllers;

use App\Http\Resources\CouponIdResource;
use App\Http\Resources\CouponShopIdResource;
use App\Http\Resources\CouponShopListResource;
use App\Http\Resources\CouponShopResource;
use App\Repositories\CouponRepository;
use App\Repositories\ShopRepository;
use App\Services\CouponService;
use App\Traits\ResponserTraits;
use Illuminate\Http\Request;

class CouponShopController extends Controller
{
    use ResponserTraits;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $begin = microtime(true);
        $data = CouponRepository::getCouponShops($request);
        $end = microtime(true) - $begin;
        return $this->respondListCollection('1', CouponShopListResource::collection($data['coupons']), [], round($end, 2), $data['limit'], $data['offset']);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $begin = microtime(true);
        $validatedData = \Validator::make($request->all(), [
            'shop_id' => 'required|integer',
            'coupon_id' => 'required|integer'
        ]);

        if ($validatedData->fails()) {
            $end = microtime(true) - $begin;
            return $this->respondValidationErrorCollection('0', [], $validatedData->errors(), round($end, 2));
        }

        $coupon = CouponRepository::getCouponByID($request->coupon_id);
        $shop = ShopRepository::getShopByID($request->shop_id);


        if (!$coupon || !$shop) {
            $end = microtime(true) - $begin;
            return $this->respondNotFoundErrorCollection('0', [], ['message' => "The resource that matches the request ID does not found.", 'code' => 404002], round($end, 2));
        }

        $couponShop = CouponService::storeCouponShop($coupon, $shop);
        if ($couponShop['code'] == 409) {
            $end = microtime(true) - $begin;
            return $this->respondNotFoundErrorCollection('0', [], ['message' => "The inserting resource was already registered.", 'code' => 409001], round($end, 2));
        }

        if ($couponShop['code'] == 200) {
            $end = microtime(true) - $begin;
            return $this->respondUpdateCollection('1', new CouponIdResource($coupon), $validatedData->errors(), round($end, 2));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($coupon_id, $shop_id)
    {
        $begin = microtime(true);
        $coupon = CouponRepository::getCouponByID($coupon_id);
        $shop = ShopRepository::getShopByID($shop_id);

        if (!$coupon || !$shop) {
            $end = microtime(true) - $begin;
            return $this->respondNotFoundErrorCollection('0', [], ['message' => "The resource that matches the request ID does not found.", 'code' => 404002], round($end, 2));
        }
        $end = microtime(true) - $begin;

        return $this->respondSuccessCollection('1', new CouponShopResource($coupon, $shop), [], round($end, 2));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($coupon_id, $shop_id)
    {
        $begin = microtime(true);
        $coupon = CouponRepository::getCouponByID($coupon_id);
        $shop = ShopRepository::getShopByID($shop_id);

        if (!$coupon || !$shop) {
            $end = microtime(true) - $begin;
            return $this->respondNotFoundErrorCollection('0', [], ['message' => "The resource that matches the request ID does not found.", 'code' => 404002], round($end, 2));
        }
        $coupon->shops()->detach($shop->id);
        $end = microtime(true) - $begin;

        return $this->respondSuccessCollection('1', ['delete' => 1], [], round($end, 2));
    }
}
