<?php

namespace App\Http\Controllers;

use App\Http\Requests\CouponRequest;
use App\Http\Resources\CouponIdResource;
use App\Http\Resources\CouponResource;
use App\Models\Admin;
use App\Models\Coupon;
use App\Repositories\AdminRepository;
use App\Repositories\CouponRepository;
use App\Services\CouponService;
use App\Traits\ResponserTraits;
use Illuminate\Http\Request;

class CouponApiController extends Controller
{
    use ResponserTraits;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $begin = microtime(true);
        $data = CouponRepository::getCoupons($request);
        $end = microtime(true) - $begin;
        return $this->respondListCollection('1', CouponResource::collection($data['coupons']), [], round($end, 2), $data['limit'], $data['offset']);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Admin $admin)
    {
        $begin = microtime(true);
        $validatedData = \Validator::make($request->all(), [
            'name' => 'required|max:128',
            'description' => 'nullable',
            'discount_type' => 'required|in:percentage,fix-amount',
            'amount' => 'required|integer',
            'image_url' => 'nullable',
            'code' => 'required|integer',
            'start_datetime' => 'nullable|date_format:Y-m-d H:i:s',
            'end_datetime' => 'nullable|date_format:Y-m-d H:i:s',
            'coupon_type' => 'required|in:private,public',
            'used_count' => 'required'
        ]);
        if ($validatedData->fails()) {
            $end = microtime(true) - $begin;
            return $this->respondValidationErrorCollection('0', [], $validatedData->errors(), round($end, 2));
        }
        $coupon = CouponService::store($request, $admin);
        $end = microtime(true) - $begin;

        return $this->respondCreateCollection('1', new CouponIdResource($coupon), $validatedData->errors(), round($end, 2));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $begin = microtime(true);
        $coupon = Coupon::find($id);
        if (!$coupon) {
            $end = microtime(true) - $begin;
            return $this->respondNotFoundErrorCollection('0', [], ['message' => "The resource that matches the request ID does not found.", 'code' => 404002], round($end, 2));
        }
        $end = microtime(true) - $begin;
        return $this->respondSuccessCollection('1', new CouponResource($coupon), [], round($end, 2));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $begin = microtime(true);
        $validatedData = \Validator::make($request->all(), [
            'name' => 'nullable|max:128',
            'description' => 'nullable',
            'discount_type' => 'nullable|in:percentage,fix-amount',
            'amount' => 'nullable|integer',
            'image_url' => 'nullable',
            'code' => 'nullable|integer',
            'start_datetime' => 'nullable|date_format:Y-m-d H:i:s',
            'end_datetime' => 'nullable|date_format:Y-m-d H:i:s',
            'coupon_type' => 'nullable|in:private,public',
            'used_count' => 'nullable'
        ]);
        if ($validatedData->fails()) {
            $end = microtime(true) - $begin;
            return $this->respondValidationErrorCollection('0', [], $validatedData->errors(), round($end, 2));
        }
        $coupon = CouponService::update($request, $id);
        if (!$coupon) {
            $end = microtime(true) - $begin;
            return $this->respondNotFoundErrorCollection('0', [], ['message' => "The resource that matches the request ID does not found.", 'code' => 404002], round($end, 2));
        }
        $end = microtime(true) - $begin;

        return $this->respondUpdateCollection('1', new CouponIdResource($coupon), $validatedData->errors(), round($end, 2));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $begin = microtime(true);
        $coupon = Coupon::find($id);
        if (!$coupon) {
            $end = microtime(true) - $begin;
            return $this->respondNotFoundErrorCollection('0', [], ['message' => "The resource that matches the request ID does not found.", 'code' => 404002], round($end, 2));
        }
        $coupon->delete();
        $end = microtime(true) - $begin;
        return $this->respondSuccessCollection('1', ['delete' => 1], [], round($end, 2));
    }
}
