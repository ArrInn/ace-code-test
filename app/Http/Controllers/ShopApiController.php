<?php

namespace App\Http\Controllers;

use App\Http\Resources\ShopIdResource;
use App\Http\Resources\ShopResource;
use App\Models\Admin;
use App\Models\Shop;
use App\Repositories\ShopRepository;
use App\Services\ShopService;
use App\Traits\ResponserTraits;
use Illuminate\Http\Request;

class ShopApiController extends Controller
{
    use ResponserTraits;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $begin = microtime(true);
        $data = ShopRepository::getShops($request);
        $end = microtime(true) - $begin;
        return $this->respondListCollection('1', ShopResource::collection($data['shops']), [], round($end, 2), $data['limit'], $data['offset']);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Admin $admin, Request $request)
    {
        $begin = microtime(true);
        $validatedData = \Validator::make($request->all(), [
            'name' => 'required|max:64',
            'query' => 'nullable|max:64',
            'latitude' => [
                'required', 'regex:/^[-]?(([0-8]?[0-9])\.(\d+))|(90(\.0+)?)$/',
            ],
            'longitude' => [
                'required', 'regex:/^[-]?((((1[0-7][0-9])|([0-9]?[0-9]))\.(\d+))|180(\.0+)?)$/',
            ],
            'zoom' => 'nullable',
        ]);

        if ($validatedData->fails()) {
            $end = microtime(true) - $begin;
            return $this->respondValidationErrorCollection('0', [], $validatedData->errors(), round($end, 2));
        }
        $shop = ShopService::store($request->all(), $admin);
        $end = microtime(true) - $begin;

        return $this->respondCreateCollection('1', new ShopIdResource($shop), $validatedData->errors(), round($end, 2));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $begin = microtime(true);
        $shop = Shop::find($id);
        if (!$shop) {
            $end = microtime(true) - $begin;
            return $this->respondNotFoundErrorCollection('0', [], ['message' => "The resource that matches the request ID does not found.", 'code' => 404002], round($end, 2));
        }
        $end = microtime(true) - $begin;
        return $this->respondSuccessCollection('1', new ShopResource($shop), [], round($end, 2));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $begin = microtime(true);
        $validatedData = \Validator::make($request->all(), [
            'name' => 'nullable|max:64',
            'query' => 'nullable|max:64',
            'latitude' => [
                'nullable', 'regex:/^[-]?(([0-8]?[0-9])\.(\d+))|(90(\.0+)?)$/',
            ],
            'longitude' => [
                'nullable', 'regex:/^[-]?((((1[0-7][0-9])|([0-9]?[0-9]))\.(\d+))|180(\.0+)?)$/',
            ],
            'zoom' => 'nullable',
        ]);
        if ($validatedData->fails()) {
            $end = microtime(true) - $begin;
            return $this->respondValidationErrorCollection('0', [], $validatedData->errors(), round($end, 2));
        }
        $shop = ShopService::update($request, $id);
        if (!$shop) {
            $end = microtime(true) - $begin;
            return $this->respondNotFoundErrorCollection('0', [], ['message' => "The resource that matches the request ID does not found.", 'code' => 404002], round($end, 2));
        }
        $end = microtime(true) - $begin;

        return $this->respondUpdateCollection('1', new ShopIdResource($shop), $validatedData->errors(), round($end, 2));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $begin = microtime(true);
        $shop = Shop::find($id);
        if (!$shop) {
            $end = microtime(true) - $begin;
            return $this->respondNotFoundErrorCollection('0', [], ['message' => "The resource that matches the request ID does not found.", 'code' => 404002], round($end, 2));
        }
        $shop->delete();
        $end = microtime(true) - $begin;
        return $this->respondSuccessCollection('1', ['delete' => 1], [], round($end, 2));
    }
}
