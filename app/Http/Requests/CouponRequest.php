<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CouponRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:128',
            'description' => 'nullable',
            'discount_type' => 'required|in:percentage,fix-amount',
            'amount' => 'required|integer',
            'image_url' => 'nullable',
            'code' => 'required|integer',
            'start_datetime' => 'nullable|date_format:Y-m-d H:i:s',
            'end_datetime' => 'nullable|date_format:Y-m-d H:i:s',
            'coupon_type' => 'required|in:private,public',
            'used_count' => 'required'
        ];
    }
}
