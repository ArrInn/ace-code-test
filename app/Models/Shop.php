<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Shop extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'admin_id',
        'name',
        'query',
        'latitude',
        'longitude',
        'zoom',
    ];

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $casts = [
        'query' => 'string',
        'zoom' => 'integer'
    ];

    public function coupons()
    {
        return $this->belongsToMany(Coupon::class, 'coupon_shops')->withTimestamps();
    }

    public function scopeWhereName($query, $name)
    {
        if ($name) {
            return $query->where('name', 'like', '%' . $name . '%');
        }

        return $query;
    }

    public function scopeWhereOffset($query, $offset)
    {
        if ($offset) {
            return $query->offset($offset);
        }

        return $query->offset(0);
    }

    public function scopeWhereLimit($query, $limit)
    {
        if ($limit) {
            return $query->limit($limit);
        }

        return $query->limit(30);
    }
}
