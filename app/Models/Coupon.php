<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Coupon extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'admin_id',
        'name',
        'description',
        'discount_type',
        'amount',
        'image_url',
        'code',
        'start_datetime',
        'end_datetime',
        'coupon_type',
        'used_count'
    ];

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    public function shops()
    {
        return $this->belongsToMany(Shop::class, 'coupon_shops')->withTimestamps()->withPivot('id');
    }

    public function scopeWhereName($query, $name)
    {
        if ($name) {
            return $query->where('name', 'like', '%' . $name . '%');
        }

        return $query;
    }

    public function scopeWhereOffset($query, $offset)
    {
        if ($offset) {
            return $query->offset($offset);
        }

        return $query->offset(0);
    }

    public function scopeWhereLimit($query, $limit)
    {
        if ($limit) {
            return $query->limit($limit);
        }

        return $query->limit(30);
    }

    public function scopeWhereId($query, $id)
    {
        if ($id) {
            return $query->where('id', $id);
        }

        return $query;
    }
}
